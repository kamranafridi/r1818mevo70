public class NumberPublisherDemo {

	public static void main(String[] args) throws InterruptedException {

		Subject sub = new Sub();

		new BinaryView(sub);
		new DecimalView(sub);
		new HexaDecimalView(sub);

		System.out.println("first Condition change: 55");
		subject.setState(55);
		System.out.println("second Condition change: 40");
		subject.setState(40);

//		BinaryView bView = new BinaryView();
//		HexaDecimalView hView = new HexaDecimalView();
//		DecimalView dView = new DecimalView();
//
//
//		NumberPublisher publisher = new 						NumberPublisher(bView, hView, dView);
//		int publishCount = 6;
//
//		for (int i =0; i<publishCount; i++){
//			int num = i*30;
//			System.out.println("\nLounching:" + num);
//			publisher.publishNumber(num);
//			Thread.sleep(2000);
//		}

	}

}
