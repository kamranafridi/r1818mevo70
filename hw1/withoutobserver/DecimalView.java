public class DecimalView extends Obs
{
	public DecimalView(Sub sub){
		this.sub = sub;
		this.sub.attach(this);
	}

	@Override
	public void update() {
		System.out.println( "Binary String: " + Integer.toBinaryString( sub.getState() ) );
	}
}