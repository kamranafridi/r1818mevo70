public class HexaDecimalView extends Obs
{
	public HexaDecimalView(Sub sub){
		this.sub = sub;
		this.sub.attach(this);
	}

	@Override
	public void update() {
		System.out.println( "Binary String: " + Integer.toBinaryString( sub.getState() ) );
	}
}